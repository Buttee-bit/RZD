import os
import telebot
from shared.models import Posts
from sqlalchemy import insert
from datebase import session_maker
from datetime import datetime
from shared.settings import TOKEN
from shared.S3_class import S3_support
from shared.loggin_setup import LoggerSetup

logger_setup = LoggerSetup('bot', __name__)
LOGGER = logger_setup.logger

token = TOKEN #TOKEN
bot = telebot.TeleBot(token)
s3 = S3_support()

ALPHABET = 'йцукенгшщзхъфывапролджэячсмитьбю qwertyuiopasdfghjklzxcvbnm1234567890'


@bot.message_handler(commands=['start'])
def start_message(message):
    LOGGER.warning(f'Запущен бот, \nmessage.chat.id:{message.chat.id}')
    bot.send_message(message.chat.id, 'Я, бот, занимаюсь анализом Ж/Д путей')
    


@bot.message_handler(content_types=['text', 'photo', 'video'], func=lambda message: message.forward_from_chat is not None)
def handle_forwarded_message(message):
    date_time_now = datetime.fromtimestamp(message.date)
    date_time_publish = datetime.fromtimestamp(message.forward_date)
    forwarded_text = message.caption
    if message.caption:
        LOGGER.info(message.caption)
    else:
        LOGGER.info(message.caption)
    info_vvst = {
            'Танки':0,
            'БМП':0,
            'Грузовики':0
            }
    if message.photo:
        file_id = message.photo[0].file_id
        file_info = bot.get_file(file_id)
        file_path = file_info.file_path
        downloaded_file = bot.download_file(file_info.file_path)
        with open(file_path, 'wb') as new_file:
            new_file.write(downloaded_file)
        url = s3.upload_file_tg(date_time_publish, file_info.file_path)
        os.remove(file_info.file_path)
        try:
            with session_maker() as sesion:
                stmt = insert(Posts)
                stmt = stmt.values(
                    source = 3,
                    title = message.caption,
                    published = date_time_publish.isoformat(),
                    found = date_time_now.isoformat(),
                    path_video = url,
                    info_vvst=info_vvst,
                )
                sesion.execute(stmt)
                sesion.commit()
                LOGGER.info('Add post:')
            response = s3.get_url(url)              
            LOGGER.warning(f"Добавлена новость\nИсточник: Бот\n{message.caption}\n{response}")
            bot.reply_to(message, 'Спасибо! Ваше пересланное сообщение получено и обработано.')

        except Exception as e:
            LOGGER.error(e, exc_info=True)

    elif message.video:
        file_id = message.video.file_id
        file_info = bot.get_file(file_id)
        file_path = file_info.file_path
        downloaded_file = bot.download_file(file_info.file_path)
        with open(file_path, 'wb') as new_file:
            new_file.write(downloaded_file)
        url = s3.upload_file_tg(date_time_publish, file_info.file_path)
        response = s3.get_url(url)              

        os.remove(file_info.file_path)
        try:
            with session_maker() as sesion:
                stmt = insert(Posts)
                stmt = stmt.values(
                    source = 3,
                    title = message.caption,
                    published = date_time_publish.isoformat(),
                    found = date_time_now.isoformat(),
                    path_video = url,
                    info_vvst=info_vvst,
                )
                sesion.execute(stmt)
                sesion.commit()
            LOGGER.warning(f"Добавлена новость\nИсточник: Бот\n{message.caption}\n{response}")
            bot.reply_to(message, 'Спасибо! Ваше пересланное сообщение получено и обработано.')

        except Exception as e:
            LOGGER.error(e)


@bot.message_handler(content_types=['photo', 'video'])
def handle_user_media(message):
    date_time_now = datetime.fromtimestamp(message.date)
    info_vvst = {
        'Танки': 0,
        'БМП': 0,
        'Грузовики': 0
    }
    if message.caption:
        title = message.caption
    else:
        title = 'Нет данных'

    if message.photo:
        file_id = message.photo[-1].file_id
        file_info = bot.get_file(file_id)
        file_path = file_info.file_path
        downloaded_file = bot.download_file(file_info.file_path)
        with open(file_path, 'wb') as new_file:
            new_file.write(downloaded_file)
        url = s3.upload_file_tg(date_time_now, file_info.file_path)
        response = s3.get_url(url)              

        os.remove(file_info.file_path)

        try:
            with session_maker() as session:
                stmt = insert(Posts)
                stmt = stmt.values(
                    source=3,
                    title=title,
                    published=date_time_now.isoformat(),
                    found=date_time_now.isoformat(),
                    path_video=url,
                    info_vvst=info_vvst,
                )
                session.execute(stmt)
                session.commit()
            LOGGER.warning(f"Добавлена новость\nИсточник: Бот\n{title}\n{response}")
            bot.reply_to(message, 'Спасибо! Ваше фото получено и обработано.')
        except Exception as e:
            LOGGER.error(e, exc_info=True)

    elif message.video:
        file_id = message.video.file_id
        file_info = bot.get_file(file_id)
        file_path = file_info.file_path
        downloaded_file = bot.download_file(file_info.file_path)
        with open(file_path, 'wb') as new_file:
            new_file.write(downloaded_file)
        url = s3.upload_file_tg(date_time_now, file_info.file_path)
        response = s3.get_url(url)              

        os.remove(file_info.file_path)

        try:
            with session_maker() as session:
                stmt = insert(Posts)
                stmt = stmt.values(
                    source=3,
                    title=title,
                    published=date_time_now.isoformat(),
                    found=date_time_now.isoformat(),
                    path_video=url,
                    info_vvst=info_vvst,
                )
                session.execute(stmt)
                session.commit()
            LOGGER.warning(f"Добавлена новость\nИсточник: Бот\n{title}\n{response}")
            bot.reply_to(message, 'Спасибо! Ваше видео получено и обработано.')
        except Exception as e:
            LOGGER.error(e)



LOGGER.info("--- bot.polling() ---")
bot.polling(none_stop = True)
