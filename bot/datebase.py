from typing import Generator
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import DeclarativeMeta, declarative_base

from shared.settings import PG_USERNAME, PG_PASSWORD, PG_HOST, PG_PORT, PG_DB

DATABASE_URL = f"postgresql://{PG_USERNAME}:{PG_PASSWORD}@{PG_HOST}:{PG_PORT}/{PG_DB}"
Base: DeclarativeMeta = declarative_base()

print(DATABASE_URL)

engine = create_engine(DATABASE_URL)
session_maker = sessionmaker(bind=engine, expire_on_commit=False)


def get_session() -> Generator:
    session = session_maker()
    try:
        yield session
    finally:
        session.close()

