import logging
from sqlalchemy import insert, select
from shared.loggin_setup import LoggerSetup
from shared.models import *
from shared.S3_class import S3_support
from shared.postgre import session
from sqlalchemy.orm import Session
from shared.settings import TG_NAME, TG_API_ID, TG_API_HASH
from telethon import TelegramClient, events
from telethon.tl.types import DocumentAttributeVideo
import re
import time
from datetime import datetime
import pymorphy2

from shared.loggin_setup import LoggerSetup
from shared.settings import THREAD_LOLOGGER, THREAD_VVST
import html

def escape_html(text):
    return html.escape(text)
logger_setup = LoggerSetup('parsingTG', __name__)
LOGGER = logger_setup.logger
from pymystem3 import Mystem

class TelegaParsing:

    def __init__(self) -> None:
        self.name = TG_NAME
        self.api_id = TG_API_ID
        self.api_hash = TG_API_HASH
        self.session: Session = session
        self.s3 = S3_support()
        self.channels = self.get_channels()
        self.mystem = Mystem()

    def clean_text(self, text):
        text = re.sub(r'\(*http\S+\)*', '', text)
        text = re.sub(r'[^a-zA-Zа-яА-ЯёЁ0-9.,:;!?\'"()-]', ' ', text)
        text = re.sub(r'\s+', ' ', text)
        return text.strip()

    def find_vvst(self, text, threshold=0.45):
        lemmatized_keywords = {
            'эшелон': 1,
            'техника': 0.5,
            'поезд': 0.5,
            'бронетехника': 1,
            'перевозить': 0.5,
            'транспортировка': 0.5,
            'железнодорожный': 0.5,
            'железная дорога': 0.5,
            'воинский эшелон': 1.5,
            'перевозка': 0.5,
            'партия': 0.45,
            'поставка': 0.55,
            'разгрузка': 0.5,
            'перегруппировка': 1,
            'передислокация': 1,
            'переброска': 1,
            'маневр': 0.75,
            'развертывание': 0.75
        }
        total_weight = 0
        lemmas = self.mystem.lemmatize(text)
        for lemma in lemmas:
            if lemma.strip() in lemmatized_keywords:
                total_weight += lemmatized_keywords[lemma.strip()]

        if total_weight >= threshold:
            LOGGER.info(f'vvst in text, total weight: {total_weight}, text: {text}')
            return True
        else:
            LOGGER.info(f'vvst not in text, total weight: {total_weight}, text: {text}')
            return False

    def get_channels(self):
        stmt = select(Chanels)
        data_channels = self.session.execute(stmt)
        data_channels = data_channels.scalars().all()
        list_channel = [channels.name for channels in data_channels]
        return list_channel

    def parse_data(self):
        with TelegramClient(self.name, self.api_id, self.api_hash, device_model="iPhone 13 Pro Max", 
                            system_version="14.8.1", app_version="8.4", lang_code="en",
                            system_lang_code="en-US") as client:
            
            @client.on(events.NewMessage(chats=self.channels))
            async def handle_new_message(event):
                message = event.message
                date_time_now = message.date
                channel_username = message.chat.username
                message_link = f'<a href="https://t.me/{channel_username}/{message.id}">{message.text[:30]}</a>'
                try:
                    text = self.clean_text(message.text)
                    if self.find_vvst(text) == True:                                           
                        stmt = select(Chanels)
                        stmt = stmt.where(Chanels.name == f'https://t.me/{channel_username}')
                        channel = self.session.execute(stmt)
                        channel = channel.scalars().one()
                        channel_id = channel.id
                        info_vvst = {
                                'Танки':0,
                                'БМП':0,
                                'Грузовики':0
                                }
                        if message.photo:
                            photo = message.photo
                            photo_file = await client.download_media(photo)
                            path = self.s3.upload_file_tg(date_time_now, photo_file)
                            LOGGER.info(f'photo: {photo_file}')
                            stmt = insert(TelegramData)
                            stmt = stmt.values(
                                id_channel = channel_id,
                                text = text,
                                published = date_time_now,
                                path = path,
                                info_vvst = info_vvst
                            )

                            try:
                                self.session.execute(stmt)
                                self.session.commit()
                                LOGGER.warning(f'Найдена информация о перевозках ВВСТ:\nДобавлена новость: {message_link}\nВремя: {message.date}\n')
                            except Exception as e:
                                self.session.rollback()
                                LOGGER.error(f'Error while processing message: {str(e)}', exc_info=True)
                                raise
                            LOGGER.info(f'stmt: {stmt}')

                        if message.video:
                            video = message.video
                            video_file = await client.download_media(video)
                            path = self.s3.upload_file_tg(date_time_now, video_file)
                            stmt = insert(TelegramData)
                            stmt = stmt.values(
                                id_channel = channel_id,
                                text = text,
                                published = date_time_now,
                                path = path,
                                info_vvst = info_vvst
                            )
                            try:
                                self.session.execute(stmt)
                                self.session.commit()
                                LOGGER.warning(f'Найдена информация о перевозках ВВСТ:\nДобавлена новость: {message_link}\nВремя: {message.date}\n')
                            except Exception as e:
                                self.session.rollback()
                                LOGGER.error(f'Error while processing message: {str(e)}', exc_info=True)
                                raise

                except Exception as e:
                    LOGGER.error(f'Error while processing message: {str(e)}', exc_info=True)
            
            client.run_until_disconnected()


def main():
    tg = TelegaParsing()
    if __name__ == '__main__':
        LOGGER.info('Начат сбор данных из Telegram')
        tg.parse_data()

main()