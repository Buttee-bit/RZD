from natasha import (MorphVocab, LOC, AddrExtractor)

class Predictor:
    def __init__(self) -> None:
        self.addr_extractor = AddrExtractor(MorphVocab())

    def find_adress(self, text: str):
        try:
            matches = self.addr_extractor(text)
            facts = [i.fact.as_json for i in matches]
            adress_collector = dict()
            for i in range(len(facts)):
                tmp = list(facts[i].values())
                adress_collector[tmp[1]] = tmp[0]
            return adress_collector
        except Exception:
            return adress_collector
        
    def predict(self, text: str):
        adress = self.find_adress(text)

    