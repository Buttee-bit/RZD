import datetime
import re
from fastapi import APIRouter, Depends
from fastapi.responses import JSONResponse
from sqlalchemy.ext.asyncio import AsyncSession
from ..datebase import get_async_session
from fastapi import UploadFile, File
from .schemas import * 
import fastapi_users
from ..auth.base_config import fastapi_users
from ..s3_connection import s3
from fastapi import File, UploadFile, Form
import os
from ..set_loggin import LoggerSetup

from .support import Support

router = APIRouter (
    prefix='/admin',
    tags= ['admin']
)

current_user = fastapi_users.current_user()

logger_setup = LoggerSetup('backend', __name__)
LOGGER = logger_setup.logger



@router.get('/info-db')
async def get_info_db(session: AsyncSession = Depends(get_async_session)):
    LOGGER.info("Start info-db")
    support = Support(session, s3)
    LOGGER.info("Start support.info_db()")
    data = await support.info_db()
    # LOGGER.info(f'data: {data}')
    return JSONResponse(content=data)

@router.get('/info-all-admin')
async def info_all_admin(session: AsyncSession = Depends(get_async_session)):
    LOGGER.info("Start info-all-admin")
    support = Support(session, s3)
    LOGGER.info("Start support.info_all_admin()")
    data = await support.info_all_admin()
    # LOGGER.info(f"data: {data}")

    return JSONResponse(content=data)

@router.get('/GetEditPost/{id}')
async def get_post_id(id:int,
                      
                      session: AsyncSession = Depends(get_async_session)):
    
    support = Support(session, s3)
    data = await support.get_post_id(id)
    return JSONResponse(content=data)



@router.put('/GetEditPost/{id}')
async def put_post_id(PutPost:PutPost,
                      
                      session: AsyncSession = Depends(get_async_session)):
    support = Support(session, s3)
    data = await support.put_post_id(PutPost)
    return JSONResponse(content=data)

@router.post('/addPost')
async def add_post(title: str = Form(...),
                   sorce: str = Form(...),
                   Latitude: float = Form(None),
                   Longitude: float = Form(None),
                   photo: UploadFile = File(...),
                   
                   session: AsyncSession = Depends(get_async_session)):
            
    title = re.sub(r"’", '', title)
    formatted_published = datetime.datetime.utcnow().strftime("%Y-%m-%d-")
    folder_path = os.getcwd() + f'/src/parsing/reddit/preview'
    file_path = os.path.join(folder_path, formatted_published+title[0:12]+'.jpg')
    
    with open(file_path, "wb") as f:  
        f.write(await photo.read())

    info = (title, #title
            sorce, #sorce
            formatted_published+title[0:12], #path_video
            Latitude, #Latitude
            Longitude, #Longitude
            ) 
    
    support = Support(session, s3)
    await support.add_post(info)
    return JSONResponse(content=['Пост создан'])

@router.delete('/DeletePost/{id}')
async def delete_post(id:int,
                      
                      session: AsyncSession = Depends(get_async_session)):
    support = Support(session, s3)
    await support.delete_for_id(id)
    return JSONResponse(content=[{
        'msg':f'Post №{id} delete'
    }])

@router.get('/all_suggest')
async def get_sugest_posts(session: AsyncSession = Depends(get_async_session),
                           User = Depends(current_user)):
    support = Support(session, s3)
    res = await support.get_sugest_posts()
    return res

@router.get('/counts_suggest')
async def get_sugest_posts(session: AsyncSession = Depends(get_async_session),
                           User = Depends(current_user)):
    support = Support(session, s3)
    res = await support.counts_suggest()
    return res



@router.put('/accept_suggest/{source}/{id_suggest}') #PUT
async def accept_suggest(id_suggest:int,
                         source:str, 
                         session: AsyncSession = Depends(get_async_session)):
    support = Support(session, s3)
    await support.accept_suggest(source, id_suggest)
    return 'res'

@router.delete('/delete_suggest/{source}/{id_suggest}')
async def delete_suggest(id_suggest:int,
                         source:str,
                         session: AsyncSession = Depends(get_async_session)):
    support = Support(session, s3)
    await support.delete_suggest(source, id_suggest)
    return 'delete_suggest'


# @router.get('info_page')
# async def get_info_page(session: AsyncSession = Depends(get_async_session)):
#     ...

@router.get('/get_all_data')
async def get_all_data(session: AsyncSession = Depends(get_async_session)):
    support = Support(session, s3)
    res = await support.get_all_data()
    print(res)

@router.get('/to_bd_all_data')
async def to_bd_all_data(session: AsyncSession = Depends(get_async_session)):
    support = Support(session, s3)
    res = await support.to_bd_all_data()
    print(res)