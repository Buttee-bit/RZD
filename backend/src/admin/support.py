import datetime
import logging
import os
import pickle
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy import and_, delete, desc, insert, select,  func, update 
from .schemas import *
from  ..auth.models import *
from sqlalchemy.orm import selectinload
from ..config import BUCKET
from ..set_loggin import LoggerSetup

logger_setup = LoggerSetup('backend', __name__)
LOGGER = logger_setup.logger

class Support:

    def __init__(self, coonection, s3):
        self.connect: AsyncSession = coonection
        self.s3 = s3

    async def info_db(self):
        stmt_post_count = select(func.count(Posts.name_sourse))
        stmt_post_count_tg = select(func.count(TelegramData.id_channel))
        
        source_count_Subreddits = select(func.count(Subreddits.id))
        source_count_Chanels = select(func.count(Chanels.id))

        stmt_suggest_count = (select(Posts).where(and_
                       (Posts.is_tank == False,
                        Posts.processed == False)
                       ))
        stmt_suggest_count_tg = (select(TelegramData).where(
                        TelegramData.processed == False))
        stmt_post_count_tg = await self.connect.execute(stmt_post_count_tg)
        suggest_count = await self.connect.execute(stmt_suggest_count)
        post_count = await self.connect.execute(stmt_post_count)
        source_count_Subreddits = await self.connect.execute(source_count_Subreddits)
        source_count_Chanels = await self.connect.execute(source_count_Chanels)
        stmt_suggest_count_tg = await self.connect.execute(stmt_suggest_count_tg)
        suggest_count = suggest_count.fetchall() 
        stmt_suggest_count_tg = stmt_suggest_count_tg.fetchall()
        return ([
            {"post_count": post_count.fetchone()[0]+stmt_post_count_tg.fetchone()[0],
            "source_count": source_count_Subreddits.fetchone()[0]+source_count_Chanels.fetchone()[0]+1,
            "suggest_count": len(suggest_count)+len(stmt_suggest_count_tg)
            }
                  ])
    
    async def info_all_admin(self):
        stmt = select(Posts)
        stmt = stmt.where(and_(Posts.is_tank == True,
                        Posts.processed == True))
        stmt = stmt.options(selectinload(Posts.name_sourse))
        stmt = stmt.order_by(desc(Posts.published))
        res = await self.connect.execute(stmt)
        res = res.scalars().all()
        data = []
        for i in res:
            response = self.s3.generate_presigned_url('get_object',
                                                    Params={'Bucket': BUCKET,
                                                            'Key': i.path_video},
                                                    ExpiresIn=1800)
            object_ = {
                    'source': i.name_sourse.title,
                    'title': i.title,
                    'published': i.published.replace(tzinfo=None).strftime('%Y-%m-%d %H:%M:%S') if i.published else None,
                    'Latitude': i.Latitude,
                    'Longitude': i.Longitude,
                    'sourceIcon': f'http://95.140.146.208/api/icons/{i.name_sourse.title}.svg',
                    'photoUrl': response,
                    'id': i.id_post,
                    'info_vvst':i.info_vvst
                    }
            data.append(object_)
        return data 

    
    async def get_post_id(self, id):
        stmt = (
        select(Posts.title, Posts.published, Posts.path_video, Posts.Latitude, Posts.Longitude,  Sources.title, Posts.id_post,Posts.info_vvst)
        .where(
            and_(Posts.is_tank == True,
                 Posts.id_post == id,
                )
            )
        .join(Sources)
    )
    
        res = await self.connect.execute(stmt)
        res = res.fetchall()
        data = await self.all_data_for_bd(res)
        return data

    async def put_post_id(self,PutPost:PutPost ):
        stmt = (
        update(Posts)
        .where(Posts.id_post == PutPost.id_post)
        .values(title = PutPost.title,
                info_vvst=PutPost.info_vvst,
                Latitude = PutPost.Latitude,
                Longitude= PutPost.Longitude,
                )
    )
        await self.connect.execute(stmt)
        await self.connect.commit()

        return {f'{PutPost.id_post} изменен'}  
    

    async def add_post(self, info):
        dict_vvst = {
             'Танки':0,
             'БМП':0,
             'Грузовики':0
        }
        litle_select = (
            select(Sources.id_source)
            .where(Sources.title == info[1] )
        )
        sorce_ = await self.connect.execute(litle_select)
        sorce_ = sorce_.fetchone()
        stmt = (insert(Posts)
                .values(
                    sorce = sorce_[0],
                    title = info[0],
                    published = datetime.datetime.utcnow(),
                    found = datetime.datetime.utcnow(),
                    path_video = info[2],
                    is_tank = True,
                    is_train = True,
                    info_vvst = dict_vvst,
                    Latitude = info[3],
                    Longitude = info[4],
                    processed = True)
                    )
        
        await self.connect.execute(stmt)
        await self.connect.commit()        

    async def delete_for_id(self, id:int):
        folder_path_video = os.getcwd() + r'\reddit_video\\'
        folder_path_photo = os.getcwd() + r'\preview\\'
        stmt = (delete(Posts)
                .where(Posts.id_post == id))
        stmt_for_del_file = (
            select(Posts.path_video)
            .where(Posts.id_post == id)
        )
        name_file = await self.connect.execute(stmt_for_del_file)
        name_file = name_file.fetchone()[0]
        try:
            os.remove(folder_path_video+name_file+'.mp4')
        except:
            pass
        try:
            os.remove(folder_path_photo+name_file+'.jpg')
        except:
            pass
        await self.connect.execute(stmt)
        await self.connect.commit()

    async def get_sugest_posts(self):
        posts_stmt = (
            select(Posts)
            .where(and_(Posts.is_tank == False, Posts.processed == False))
            .options(selectinload(Posts.name_sourse))
            .order_by(desc(Posts.published))
        )
        posts_res = await self.connect.execute(posts_stmt)
        posts_data = posts_res.scalars().all()

        tg_stmt = (
            select(TelegramData)
            .where(TelegramData.processed == False)
            .order_by(desc(TelegramData.published))
        )
        tg_res = await self.connect.execute(tg_stmt)
        tg_data = tg_res.scalars().all()

        combined_data = [
            {
                'source': i.name_sourse.title,
                'title': i.title,
                'published': i.published.replace(tzinfo=None).strftime('%Y-%m-%d %H:%M:%S') if i.published else None,
                'Latitude': i.Latitude,
                'Longitude': i.Longitude,
                'sourceIcon': f'http://95.140.146.208/api/icons/{i.name_sourse.title}.svg',
                'photoUrl': self.get_photo_url(i.path_video),
                'id': i.id_post,
                'info_vvst': i.info_vvst
            }
            for i in posts_data
        ] + [
            {
                'source': 'Telegram',
                'title': i.text,
                'published': i.published.replace(tzinfo=None).strftime('%Y-%m-%d %H:%M:%S') if i.published else None,
                'Latitude': i.Latitude,
                'Longitude': i.Longitude,
                'sourceIcon': 'http://95.140.146.208/api/icons/Telegram.svg',
                'photoUrl': self.get_photo_url(i.path),
                'id': i.id,
                'info_vvst': i.info_vvst
            }
            for i in tg_data
        ]

        sorted_data = sorted(combined_data, key=lambda x: x['published'], reverse=True)

        return sorted_data

    def get_photo_url(self, path):
        try:
            if 'https' not in path:
                return self.s3.generate_presigned_url('get_object', Params={'Bucket': BUCKET, 'Key': path}, ExpiresIn=1800)
            else:
                return path
        except Exception as e:
            return path

    async def counts_suggest(self):
        stmt = select(Posts)
        stmt = stmt.options(selectinload(Posts.name_sourse))
        stmt = stmt.where(Posts.processed == False)
        stmt = stmt.order_by(desc(Posts.published))
        res = await self.connect.execute(stmt)
        res = res.scalars().all()

        data = {
            'Reddit':0,
            'Telegram':0,
            'Bot':0
        }

        for i in res:
            if i.source == 1:
                data['Reddit'] +=1
            elif i.source == 2:
                data['Telegram'] +=1
            else:
                data['Bot'] +=1
                
        stmt = select(TelegramData)
        stmt = stmt.where(TelegramData.processed == False)
        res = await self.connect.execute(stmt)
        res = res.scalars().all()

        data['Telegram'] =+ len(res)
        
        return data

# 
    async def accept_suggest(self,source:str, id:int):
        if source == 'Reddit' or source == 'Bot':
            stmt = update(Posts)
            stmt = stmt.where(Posts.id_post == id)
            stmt = stmt.values(
                is_tank = True,
                is_train = True,
                processed = True,
            )
            await self.connect.execute(stmt)
            await self.connect.commit()
            
        else:
            stmt = select(TelegramData)
            stmt = stmt.where(TelegramData.id == id)
            res = await self.connect.execute(stmt)
            res = res.scalars().one()

            stmt = insert(Posts)
            stmt = stmt.values(
                source = 2,
                title = res.text,
                published = res.published,
                found = res.found,
                path_video = res.path,
                is_tank = True,
                is_train = True,
                info_vvst = res.info_vvst,
                processed = True,
            )
            await self.connect.execute(stmt)
            await self.connect.commit()


            stmt = update(TelegramData)
            stmt = stmt.where(TelegramData.id == id)
            stmt = stmt.values(
                processed = True,
            )
            await self.connect.execute(stmt)
            await self.connect.commit()

        return 'Ты принял accept_suggest'
    

    async def delete_suggest(self,source:str, id):
        if source == 'Reddit':
            stmt = delete(Posts)
            stmt = stmt.where(Posts.id_post == id)
            await self.connect.execute(stmt)
            await self.connect.commit()
            return 'Отклонен пост'
        else:
            stmt = delete(TelegramData)
            stmt = stmt.where(TelegramData.id == id)
            await self.connect.execute(stmt)
            await self.connect.commit()
            return 'Отклонен пост'
        
    async def all_data_for_bd(self, list_data):
        data = []
        if len(list_data) == 1:
            if 'TG' in list_data[0][2]:
                photoUrl =  f'http://95.140.146.208/api:8001/Photos/{list_data[0][2]}.jpg',
            else:
                photoUrl = f'http://95.140.146.208/api/preview/{list_data[0][2]}.jpg',
            object_ = {
                        'source': list_data[0][5],
                        'title': list_data[0][0],
                        'published': str(list_data[0][1]).split(' ')[0],
                        'Latitude': list_data[0][3],
                        'Longitude': list_data[0][4],
                        'sourceIcon': f'http://95.140.146.208/api/icons/{list_data[0][5]}.svg',
                        'photoUrl': photoUrl,
                        'id':list_data[0][6],
                        'info_vvst':list_data[0][-1]
                        }
            return object_ 
        else:
            for i in list_data:
                if 'TG' in i[2]:
                    photoUrl =  f'http://95.140.146.208/api:8001/Photos/{i[2]}.jpg',
                else:
                    photoUrl = f'http://95.140.146.208/api/preview/{i[2]}.jpg',
                object_ = {
                        'source': i[5],
                        'title': i[0],
                        'published': str(i[1]).split(' ')[0],
                        'Latitude': i[3],
                        'Longitude': i[4],
                        'sourceIcon': f'http://95.140.146.208/api/icons/{i[5]}.svg',
                        'photoUrl': photoUrl,
                        'id':i[6],
                        'info_vvst':i[-1]
                        }
                data.append(object_)
        return data



    async def get_all_data(self):
        data = []
        stmt = select(Posts)
        res = await self.connect.execute(stmt)
        for i in res:
            data.append(i)

        with open("Posts.pickle", "wb") as file:
            pickle.dump(data, file)

        data = []
        stmt = select(NewsData)
        res = await self.connect.execute(stmt)
        for i in res:
            data.append(i)
        with open("NewsData.pickle", "wb") as file:
            pickle.dump(data, file)


    async def to_bd_all_data(self):
        with open('NewsData.pickle', 'rb') as f:
            loaded_obj = pickle.load(f)
            for i in loaded_obj:
                stmt = insert(NewsData).values(
                    message_id = i[0].message_id,
                    sender = i[0].sender,
                    chat_title = i[0].chat_title,
                    date = i[0].date,
                    message = i[0].message,
                    path = i[0].path,
                    info_vvst = i[0].info_vvst,
                    Latitude = i[0].Latitude,
                    Longitude = i[0].Longitude,
                    processed = i[0].processed,
                )
                await self.connect.execute(stmt)
                await self.connect.commit()
                    
        with open('Posts.pickle', 'rb') as f:
            loaded_obj = pickle.load(f)
            for i in loaded_obj:
                stmt = insert(Posts).values(
                    source=i[0].source,
                    title=i[0].title,
                    published=i[0].published,
                    found=i[0].found,
                    path_video=i[0].path_video,
                    is_tank=i[0].is_tank,
                    is_train=i[0].is_train,
                    info_vvst=i[0].info_vvst,
                    Latitude=i[0].Latitude,
                    Longitude=i[0].Longitude,
                    processed=i[0].processed,
                )
                await self.connect.execute(stmt)
                await self.connect.commit()