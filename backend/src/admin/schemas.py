from pydantic import BaseModel
from typing import Any, Dict, List, Optional
from fastapi import UploadFile, File





class GetInfoDB(BaseModel):
    post_count : int
    source_count : int
    suggest_count : int

class PhotoUrl(BaseModel):
    url: str

class InfoVvst(BaseModel):
    Танки: Optional[int] = 0
    БМП: Optional[int] = 0
    Грузовики: Optional[int] = 0

class DataItem(BaseModel):
    source: str
    title: str
    published: str
    Latitude: Optional[float] = None
    Longitude: Optional[float] = None
    sourceIcon: str
    photoUrl: List[str]
    id: int
    info_vvst: InfoVvst

class PydanticModel(BaseModel):
    data: List[DataItem]






class PutPost(BaseModel):
    id_post: int
    title: str
    info_vvst: Dict={
        'танки': 0, 
        'бмп': 0, 
        'грузовики': 0
    }
    Latitude: float | None
    Longitude: float | None 

# class AddPost(BaseModel):
#     title: str
#     sorce: str
#     Latitude: float | None
#     Longitude: float | None
#     info_vvst: dict
#     photo: UploadFile


class SuggestPost(BaseModel):
    title: str
    sorce: str
    Latitude: float | None
    Longitude: float | None
    info_vvst: dict
    photo: str | None

class DelPost(BaseModel):
    id: int
