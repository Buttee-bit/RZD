
from fastapi import APIRouter,Depends
from fastapi.responses import JSONResponse
from sqlalchemy.ext.asyncio import AsyncSession
from ..datebase import get_async_session
from ..admin.support import Support
from ..s3_connection import s3

router = APIRouter(
    prefix='/find_city',
    tags = ['find_city']
)

@router.put('/find_city')
async def find_city(session: AsyncSession = Depends(get_async_session)):
    support = Support(session, s3)
    await support.find_city()
    return JSONResponse(
        content={
            'msg':'complete'
        }
    )

@router.put('/lat_lon')
async def lat_lon(session: AsyncSession = Depends(get_async_session)):
    support = Support(session, s3)
    await support.lat_lon_m()
    return JSONResponse(
        content={
            'msg':'complete'
        }
    )


@router.put('/TEST_LOCATION')
async def test_locations(session: AsyncSession = Depends(get_async_session)):
    support = Support(session, s3)
    await support.test_locations()
    return JSONResponse(
        content={
            'msg':'complete'
        }
    )