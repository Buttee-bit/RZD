from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from prometheus_fastapi_instrumentator import Instrumentator
from .set_loggin import LoggerSetup


logger_setup = LoggerSetup('backend', __name__)
LOGGER = logger_setup.logger
from .auth.router import router as router_auth
from .admin.router import router as router_admin
from .find_city.router import router as router_find_city

app = FastAPI(
    title="VKR API"
)

origins = [
    "http://localhost",
    "http://95.140.146.208",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.on_event("startup")
async def startup():
    LOGGER.info("--- Start up App ---")
    pass

@app.on_event("shutdown")
async def shutdown():
    LOGGER.info("--- shutdown App ---")
    pass

# app.mount("/photos", StaticFiles(directory="photos"), name="photos")
app.mount("/icons", StaticFiles(directory="icons"), name="icons")



app.include_router(router_auth)
app.include_router(router_admin)
app.include_router(router_find_city)

Instrumentator().instrument(app).expose(app)
