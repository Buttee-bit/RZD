from __future__ import annotations

from datetime import datetime
from fastapi_users_db_sqlalchemy import SQLAlchemyBaseUserTable

from sqlalchemy import (JSON, TIMESTAMP, BigInteger, Boolean, DateTime, Float, ForeignKey, Integer, MetaData, String)
                        
from sqlalchemy.dialects.postgresql import ARRAY
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column, relationship
from sqlalchemy.sql import func
from sqlalchemy.sql import func

from sqlalchemy import DateTime, func
from sqlalchemy.dialects.postgresql import TIMESTAMP
from sqlalchemy.orm import Mapped, mapped_column



class Base(DeclarativeBase):
    """Base class"""

    metadata = MetaData(
        naming_convention={
            'ix': 'ix_%(column_0_label)s',
            'uq': 'uq_%(table_name)s_%(column_0_name)s',
            'ck': 'ck_%(table_name)s_`%(constraint_name)s`',
            'fk': 'fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s',
            'pk': 'pk_%(table_name)s',
        }
    )

class Role(Base):
    __tablename__ = 'roles'

    id: Mapped[int] = mapped_column(
        'id',
        Integer,
        primary_key=True,
        autoincrement=True
    )

    name: Mapped[int] = mapped_column(
        'name',
        String,
        unique=True
    )

class User(SQLAlchemyBaseUserTable[int], Base): #
    __tablename__ = 'users'

    id: Mapped[int] = mapped_column(
        'id', 
        BigInteger, 
        primary_key=True,
        autoincrement=True,
        )

    name: Mapped[str] = mapped_column(
        'name',
        String, 
        nullable=True
    )

    email: Mapped[str] = mapped_column(
        'email',
        String,
        nullable=True
    )

    hashed_password: Mapped[str] = mapped_column(
        'hashed_password',
        String(length=1024),
        nullable=True
    )

    is_active: Mapped[bool] = mapped_column(
        'is_active',
        Boolean,
        default=True,
        nullable=False
    )

    is_superuser: Mapped[bool] = mapped_column(
        'is_superuser',
        Boolean,
        default=False,
        nullable=False
    )

    is_verified: Mapped[bool] = mapped_column(
        'is_verified',
        Boolean,
        default=False,
        nullable=False
    )

    register_at: Mapped[datetime] = mapped_column(
        'register_at',
        DateTime, 
        server_default=func.now(),
        nullable=False
    )

    role_id: Mapped[int] = mapped_column(
        'role_id',
        ForeignKey(Role.id)
    )


class Sources(Base):
    __tablename__ = 'source'

    id_source:Mapped[int] = mapped_column(
        'id_source',
        Integer,
        primary_key=True
    )

    title:Mapped[String] = mapped_column(
        'title',
        String
    )

    posts:Mapped[list['Posts']] = relationship('Posts', back_populates='name_sourse')


class Posts(Base):
    __tablename__ = 'post'

    id_post: Mapped[int] = mapped_column(
        'id_post',
        Integer,
        primary_key=True,
        autoincrement=True,
    )

    source: Mapped[int] = mapped_column(
        'source',
        ForeignKey(Sources.id_source)
    )
    title: Mapped[str] = mapped_column(
        'title',
        String
    )
    published: Mapped[datetime] = mapped_column(
        'published',
        DateTime
    )
    found: Mapped[datetime] = mapped_column(
        'found',
        TIMESTAMP(timezone=True),
        server_default=func.now().op('AT TIME ZONE')('Europe/Moscow')
    )
    path_video: Mapped[str] = mapped_column(
        'path_video',
        String,
    )
    is_tank: Mapped[bool] = mapped_column(
        'is_tank',
        Boolean,
        default=False
    )
    is_train: Mapped[bool] = mapped_column(
        'is_train',
        Boolean,
        default=False
    )
    info_vvst: Mapped[JSON] = mapped_column(
        'info_vvst',
        JSON,
        nullable=True,
    )
    Latitude: Mapped[float] = mapped_column(
        'Latitude',
        Float,
        nullable=True,

    )
    Longitude: Mapped[float] = mapped_column(
        'Longitude',
        Float,
        nullable=True,
    )
    processed: Mapped[bool] = mapped_column(
        'processed',
        Boolean,
        default=False
    )
    name_sourse:Mapped['Sources'] = relationship()

class NewsData(Base):
    __tablename__ = 'news_data'

    tg_data_id: Mapped[int] = mapped_column(
        'tg_data_id',
        Integer,
        primary_key=True,
        autoincrement=True,
    )

    message_id: Mapped[int] = mapped_column(
        'message_id',
        Integer,
    )

    sender: Mapped[str] = mapped_column(
        'sender',
        String,
    )

    chat_title: Mapped[str] = mapped_column(
        'chat_title',
        String,
    )

    date: Mapped[datetime] = mapped_column(
        'date',
        DateTime,
    )

    message: Mapped[str] = mapped_column(
        'message',
        String,
    )

    path: Mapped[str] = mapped_column(
        'path',
        String,
    )

    info_vvst: Mapped[JSON] = mapped_column(
        'info_vvst',
        JSON,
        nullable=True,
    )

    Latitude: Mapped[float] = mapped_column(
        'Latitude',
        Float,
        nullable=True,
    )

    Longitude: Mapped[float] = mapped_column(
        'Longitude',
        Float,
        nullable=True,
    )

    processed: Mapped[bool] = mapped_column(
        'processed',
        Boolean,
        default=False
    )


class Subreddits(Base):
    __tablename__ = 'subreddit'

    id: Mapped[int] = mapped_column(
        'id',
        Integer,
        primary_key=True,
        autoincrement=True,
    )

    title: Mapped[str] = mapped_column(
        'title',
        String,
        nullable=False
    )

class Cityes(Base):
    __tablename__ = 'city'
    id: Mapped[int] = mapped_column(
        'id',
        Integer,
        primary_key=True,
        autoincrement=True,
    )
    country:Mapped[str] = mapped_column(
        'country',
        String
    )
    name:Mapped[str] = mapped_column(
        'name',
        String
    )
    lat:Mapped[float] = mapped_column(
        'lat',
        Float
    )
    lng:Mapped[float] = mapped_column(
        'lng',
        Float
    )


class Chanels(Base):
    __tablename__ = 'channel'

    id: Mapped[int] = mapped_column(
        'id',
        Integer,
        primary_key=True,
        autoincrement=True,
    )

    name: Mapped[str] = mapped_column(
        'name',
        String,
        nullable=True
    )

    telegram_data = relationship("TelegramData", back_populates="channel")

class TelegramData(Base):
    __tablename__ = 'telegram_data'
    
    id: Mapped[int] = mapped_column(
        'id',
        Integer,
        primary_key=True,
        autoincrement=True,
    )
    id_channel: Mapped[int] = mapped_column(
        'id_channel', 
        Integer,
        ForeignKey(Chanels.id)
    )
    
    text: Mapped[str] = mapped_column(
        'text',
        String,
        nullable=True
    )

    published: Mapped[datetime] = mapped_column(
        'published',
        DateTime
    )
    found: Mapped[datetime] = mapped_column(
        'found',
        TIMESTAMP(timezone=True),
        server_default=func.now().op('AT TIME ZONE')('Europe/Moscow')
    )

    path: Mapped[str] = mapped_column(
        'path',
        String,
    )

    info_vvst: Mapped[JSON] = mapped_column(
        'info_vvst',
        JSON,
        nullable=True,
    )

    Latitude: Mapped[float] = mapped_column(
        'Latitude',
        Float,
        nullable=True,
    )

    Longitude: Mapped[float] = mapped_column(
        'Longitude',
        Float,
        nullable=True,
    )

    processed: Mapped[bool] = mapped_column(
        'processed',
        Boolean,
        default=False
    )

    channel = relationship("Chanels", back_populates="telegram_data")