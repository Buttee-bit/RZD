from urllib.parse import urlunsplit
from sqlalchemy import create_engine
from sqlalchemy.ext.asyncio import create_async_engine, async_sessionmaker
from sqlalchemy.orm import sessionmaker
from shared.settings import *

def get_pg_uri_async(
        username=PG_USERNAME,
        password=PG_PASSWORD,
        host=PG_HOST,
        port=PG_PORT,
        db=PG_DB,
        protocol=PG_PROTOCOL_ASYNC,
        uri_query=PG_URI_QUERY
):
    return urlunsplit((protocol, f'{username}:{password}@{host}:{port}', db, uri_query, str()))


def get_pg_uri_sync(
        username=PG_USERNAME,
        password=PG_PASSWORD,
        host=PG_HOST,
        port=PG_PORT,
        db=PG_DB,
        protocol=PG_PROTOCOL_SYNC,
        uri_query=PG_URI_QUERY
):
    return urlunsplit((protocol, f'{username}:{password}@{host}:{port}', db, uri_query, str()))


async_engine = create_async_engine(get_pg_uri_async())
async_session = async_sessionmaker(async_engine)
async_session_noauto = async_sessionmaker(async_engine, autocommit=False, autoflush=False, expire_on_commit=False)

engine = create_engine(get_pg_uri_sync())
Session = sessionmaker(bind=engine)
session = Session()