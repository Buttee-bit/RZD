from pathlib import Path

from dotenv import load_dotenv
from envparse import env

load_dotenv(env('DOTENV_FILE', default=None))


BASE_DIR = Path(__file__).resolve().parent.parent

# redis
REDIS_HOST = env('REDIS_HOST', default='127.0.0.1')
REDIS_DB = env('REDIS_DB', default='redis')

# postgresql
PG_USERNAME = env('POSTGRES_USER')
PG_PASSWORD = env('POSTGRES_PASSWORD')
PG_HOST = env('DB_HOST', default='127.0.0.1')
PG_PORT = env.int('DB_PORT', default=5432)
PG_DB = env('POSTGRES_DB')
PG_PROTOCOL_ASYNC = env('POSTGRES_PROTOCOL', default='postgresql+asyncpg')
PG_PROTOCOL_SYNC = env('POSTGRES_PROTOCOL', default='postgresql')
PG_URI_QUERY = env('POSTGRES_URI_QUERY', default=str())

#BOT
TOKEN = env('TOKEN')
TOKEN_LOGGER_BOT = env('TOKEN_LOGGER_BOT')
LINK_GROUP = env('LINK_GROUP')
THREAD_LOLOGGER = env('THREAD_LOLOGGER')
THREAD_VVST = env('THREAD_VVST')
THREAD_ERROR= env('THREAD_ERROR')
#S3
BUCKET = env('BUCKET')

#ParserBot
TG_NAME = env('TG_NAME')
TG_API_ID = env('TG_API_ID')
TG_API_HASH = env('TG_API_HASH')

