import logging
import logging.handlers
from shared.CustomTelegramhandler import TelegramHandler
from shared.CustomTelegramFormatter import HtmlFormatter, HtmlForObjectFormatter 
from shared.settings import LINK_GROUP, TOKEN_LOGGER_BOT, THREAD_LOLOGGER, THREAD_VVST, THREAD_ERROR

class SingleLevelFilter(logging.Filter):
    def __init__(self, level):
        self.level = level

    def filter(self, record):
        return record.levelno == self.level

class LoggerSetup:
    def __init__(self, tag:str, logger_name) -> None:
        self.tag = tag
        self.logger = logging.getLogger(logger_name)
        self.setup_logging()

    def setup_logging(self):
        LOG_FORMAT = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
        formatter = logging.Formatter(LOG_FORMAT)
        console = logging.StreamHandler()
        console.setFormatter(formatter)
        log_file = f"logs/{self.tag}.log"
        file = logging.handlers.TimedRotatingFileHandler(filename=log_file)
        file.setFormatter(formatter)
        
        telegram_handler_info = TelegramHandler(level=logging.INFO, token=TOKEN_LOGGER_BOT, chat_id=LINK_GROUP, message_thread_id=THREAD_LOLOGGER)
        telegram_handler_info.setFormatter(HtmlFormatter())
        telegram_handler_info.addFilter(SingleLevelFilter(logging.INFO))
        self.logger.addHandler(telegram_handler_info)
        
        telegram_handler_warning = TelegramHandler(level=logging.WARNING, token=TOKEN_LOGGER_BOT, chat_id=LINK_GROUP, message_thread_id=THREAD_VVST)
        telegram_handler_warning.setFormatter(HtmlForObjectFormatter())
        telegram_handler_warning.addFilter(SingleLevelFilter(logging.WARNING))
        self.logger.addHandler(telegram_handler_warning)
        
        telegram_handler_error = TelegramHandler(level=logging.ERROR, token=TOKEN_LOGGER_BOT, chat_id=LINK_GROUP, message_thread_id=THREAD_ERROR)
        telegram_handler_error.setFormatter(HtmlFormatter())
        telegram_handler_error.addFilter(SingleLevelFilter(logging.ERROR))
        self.logger.addHandler(telegram_handler_error)

        self.logger.addHandler(console)
        self.logger.addHandler(file)
        self.logger.setLevel(logging.DEBUG)