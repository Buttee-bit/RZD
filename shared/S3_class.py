import logging
import boto3

from datetime import datetime
from shared.settings import BUCKET
from shared.loggin_setup import LoggerSetup


logger_setup = LoggerSetup('S3', __name__)
LOGGER = logger_setup.logger

class S3_support:
    def __init__(self) -> None:
        self.s3 = self.connection()
        self.BUCKET = BUCKET

    def connection(self):
        session = boto3.session.Session()
        s3 = session.client(
            service_name='s3',
            endpoint_url='https://storage.yandexcloud.net'
        )
        return s3
    
    def upload_file(self, format_published, text):
        file_name = f'bot_data/{format_published}-{text}.mp4'
        self.s3.upload_file(file_name, BUCKET, file_name)
        self.s3.close()

    def upload_file_tg(self, time:datetime, name_file):
        LOGGER.info(f'time: {time} name_file: {name_file}')
        file_name = f"tg_/{time.year}-{time.month}-{time.day}-{name_file.split('/')[-1]}"
        self.s3.upload_file(name_file, BUCKET, file_name)
        self.s3.close()
        print(self.BUCKET)
        return file_name

    def get_file(self, path_file: str):
        return self.s3.get_object(self.BUCKET, Key=path_file)
    
    def get_url(self, path_file:str):
        response = self.s3.generate_presigned_url('get_object',
                                            Params={'Bucket': BUCKET,
                                                    'Key': path_file},
                                            ExpiresIn=1800)
        return response      
