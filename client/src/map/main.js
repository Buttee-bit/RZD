import Vue from 'vue'
import VueYandexMaps from 'vue-yandex-maps'
import './axios'

Vue.use(VueYandexMaps, {
    apiKey: '244191e1-1ec2-4b4e-8a62-9ffed203f04a',
    lang: 'ru_RU',
    coordorder: 'latlong',
    version: '2.1'
  })