import { createApp } from "vue";

import App from './App.vue'
import VueCookies from 'vue3-cookies'
import router from './router/index'
import VueDatePicker from '@vuepic/vue-datepicker';
import '@vuepic/vue-datepicker/dist/main.css'
// import store from "./store/index";
// import Vue from 'vue'
import VueYandexMaps from 'vue-yandex-maps'
import './axios'
import { LMap, LTileLayer, LMarker } from 'vue2-leaflet'


const app = createApp(App);
app.component('l-map', LMap)
app.component('l-tile-layer', LTileLayer)
app.component('l-marker', LMarker)
app.component('VueDatePicker', VueDatePicker);
app.use(VueYandexMaps, {
    apiKey: 'https://api-maps.yandex.ru/2.1/?apikey=244191e1-1ec2-4b4e-8a62-9ffed203f04a&lang=ru_RU',
    lang: 'ru_RU',
    coordorder: 'latlong',
    version: '2.1'
  })
app.use(router);
// app.use(store);
app.use(VueCookies)
app.mount('#app')

// createApp(App).use(router).use(store).mount('#app')