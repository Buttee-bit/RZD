import { createRouter, createWebHistory } from "vue-router";

// import MainPage from '../pages/MainPage'
import NewMainPage from '../pages/NewMainPage'
import FromCalendar from '../components/FromCalendar'


import AdminPageLogin from '../pages/AdminPages/LoginPageAdmin'
import StateBdPage from '../pages/AdminPages/StateBdPage'
import AllPost from '@/pages/AdminPages/AllPost'
import EdditPostPage from '@/pages/AdminPages/EdditPostPage'
// import NewMap from '../components/Map/NewMap'
import AddPost from '@/pages/AdminPages/AddPost'
import SuggestVue from '@/pages/AdminPages/SuggestVue'
import StatisticPage from '@/pages/AdminPages/StatisticPage'

const routes =  [ {path : '/',name:'start', component : NewMainPage },
                  {path : '/test',name:'test', component : FromCalendar },
                  {path : '/admin',name:'admin', component : AdminPageLogin },
                  {path : '/admin/db-result',name:'db-result', component : StateBdPage },
                  {path : '/admin/all-post',name:'all-post', component : AllPost },
                  {path : '/admin/suggested',name:'suggested', component : SuggestVue },
                  {path : '/admin/add-post',name:'add-post', component : AddPost },
                  {path : '/admin/eddit-post/:id',name:'eddit-post', component : EdditPostPage },
                  {path : '/admin/statistics',name:'statistics', component : StatisticPage },

]


const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router