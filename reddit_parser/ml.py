import cv2
from ultralytics import YOLO
from roboflow import Roboflow
from sqlalchemy import insert, select, update, delete
from shared.models import *
from shared.postgre import session
from shared.loggin_setup import LoggerSetup
from s3_connection import s3
from shared.settings import BUCKET

logger_setup = LoggerSetup('ml', __name__)
LOGGER = logger_setup.logger


class CV_reddit:
    def __init__(self) -> None:
        self.roboflow = self.init_robflow() 
        self.train_predict = self.init_YOLO()  
        self.session = session


    def init_robflow(self):
        rf = Roboflow(api_key="f6DQitMGIm56iOEFJXcT")
        project = rf.workspace("rzd-na5fd").project("bvb-t60nb")
        model_mil = project.version(4).model
        return model_mil

    def init_YOLO(self):
        model_train = YOLO('yolov8n.pt')
        return model_train


    def search_frame(self, source:int):
        if source == 1:
            stmt = select(Posts)
            stmt = stmt.where(Posts.processed == False)
            res = self.session.execute(stmt)
            res = res.scalars().all()
            LOGGER.warning(f'Начата обработка {len(res)} постов из Reddit')
            for i in res:
                try:
                    get_object_response = s3.get_object(Bucket=BUCKET, Key=i.path_video)
                    name_file = i.path_video.split('/')[-1] 
                    with open(name_file, 'wb') as file:
                                file.write(get_object_response['Body'].read())
                    LOGGER.info(f'Start processed Cv : name_file: {name_file}, id_pos: {i.id_post}')
                    train, military =  self.cv(name_file)
                    upd_post = update(Posts)
                    upd_post = upd_post.where(Posts.id_post == i.id_post)
                    upd_post = upd_post.values(
                        is_tank = military,
                        is_train = train,
                        processed = True,
                    )
                    self.session.execute(upd_post)
                    self.session.commit()
                    response = s3.generate_presigned_url('get_object',
                                                            Params={'Bucket': BUCKET,
                                                                    'Key': i.path_video},
                                                            ExpiresIn=1800)
                    LOGGER.warning(f'Добавлена новость:{i.title[0:10]}...\nНаличие техники: {military}\n Наличие Ж/Д: {train}\n{response}')

                    LOGGER.info(f'{i.id_post}: upd_post, is_tank : {military} is_train : {train} ')

                except Exception as e:
                    LOGGER.error(f'Exception: {e}', exc_info=True)

        if source == 2:
            stmt = select(TelegramData)
            stmt = stmt.where(TelegramData.processed == False)
            res = self.session.execute(stmt)
            res = res.scalars().all()
            LOGGER.warning(f'Начата обработка {len(res)} постов из Telegram')

            for i in res:
                LOGGER.info(f'Start ml source 2\ntext TG:{i.text}')
                try:
                    get_object_response = s3.get_object(Bucket=BUCKET, Key=i.path)
                    name_file = i.path.split('/')[-1] 
                    
                    with open(name_file, 'wb') as file:
                        file.write(get_object_response['Body'].read())

                    LOGGER.info(f'Start processed Cv : name_file: {name_file}, id_pos: {i.id}')
                    train, military =  self.cv(name_file)
                    insert_stmt = insert(Posts)
                    insert_stmt = insert_stmt.values(
                        is_tank = military,
                        is_train = train,
                        processed = True,
                        title=i.text,
                        path_video=i.path,
                        source=2,
                        published=i.published,
                        info_vvst=i.info_vvst,
                        Latitude = None, 
                        Longitude = None, 
                    )
                    self.session.execute(insert_stmt)
                    self.session.commit()
                    response = s3.generate_presigned_url('get_object',
                                                            Params={'Bucket': BUCKET,
                                                                    'Key': i.path},
                                                            ExpiresIn=1800)
                                        
                    LOGGER.warning(f'Добавлена новость:{i.text[0:10]}...\nНаличие техники: {military}\n Наличие Ж/Д: {train}\n{response}')

                    stmt_del = delete(TelegramData)
                    stmt_del = stmt_del.where(TelegramData.id == i.id)
                    self.session.execute(stmt_del)
                    self.session.commit()

                    LOGGER.info(f'{i.id}: upd_post, is_tank : {military} is_train : {train} ')

                except Exception as e:
                    LOGGER.error(f'Exception: {e}', exc_info=True)
 
    def cv(self, path_file:str):
        if path_file.split('.')[-1] !='mp4':
            cap = cv2.VideoCapture(path_file)
            success, frame = cap.read()
            result_train = self.train_predict.predict(frame) 
            for r in result_train:
                res = r.keypoints
                if res:
                     train = True
                else:
                     train = False
                      
            result_mil = self.roboflow.predict(frame, confidence=50, overlap=30)
            if result_mil.json()['predictions']:
                military = True
            else:
                military = False
            return train, military
        
        else:
            freg_frame = 10
            train = False
            military = False
            cap = cv2.VideoCapture(path_file)
            for count in range(freg_frame):
                LOGGER.info(f'count: {count}')
                success, frame = cap.read()
                result_train = self.train_predict.predict(frame)
                class_name = ''
                for r in result_train:
                    if class_name == 'train':
                        break
                    if r.boxes:
                        for box in r.boxes:
                            class_id = int(box.cls[0])
                            class_name = r.names[class_id]
                            if class_name == 'train':
                                train = True
                            else:
                                train = False

                result_mil = self.roboflow.predict(frame, confidence=50, overlap=30)
                
                if result_mil.json()['predictions']:
                    military = True
                else:
                    military = False

                if train == True and military == True:
                    return train, military
                
            return train, military
        

    def main(self):
        try:
            LOGGER.info(f'Start ML from Reddit')
            self.search_frame(source=1)
            LOGGER.info(f'Start ML from Telegram')
            self.search_frame(source=2)
        except Exception as e:
             LOGGER.error(e, exc_info=True)