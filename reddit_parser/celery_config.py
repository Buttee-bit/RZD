import asyncio
from config import REDIS_HOST, REDIS_PORT
from celery import Celery
from datebase import async_session_maker
from parsing import RedditParser
from ml import CV_reddit
from shared.loggin_setup import LoggerSetup

logger_setup = LoggerSetup('celery', __name__)
LOGGER = logger_setup.logger


celery_app = Celery('tasks', broker=f'redis://{REDIS_HOST}:{REDIS_PORT}')
celery_app.conf.timezone = 'UTC'

@celery_app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(3600.0, combined_task.s(task_name='every_hour_run'), name='every_hour_run')
    sender.add_periodic_task(1000.0, combined_task.s(task_name='ml_train_mil'), name='ml_train_mil')

@celery_app.task
def combined_task(task_name):
    if task_name == 'every_hour_run':
        LOGGER.info("Задача every_hour_run добавлена в очередь Celery!")
        try:    
            async def async_task():
                async with async_session_maker() as session:
                    parser = RedditParser(session)
                    await parser.main()

            loop = asyncio.get_event_loop()
            loop.run_until_complete(async_task())

        except Exception as e:
            LOGGER.error(f'Произошла непредвиденная ошибка:{e}', exc_info=True)

    elif task_name == 'ml_train_mil':
        LOGGER.info("Задача every_hour_run добавлена в очередь Celery!")
        try:    
            computer_vision = CV_reddit()
            computer_vision.main()
        except Exception as e:
            LOGGER.error(f'Произошла непредвиденная ошибка: {e}', exc_info=True)
