#!/bin/bash

if [[ "${1}" == "celery" ]]; then
  celery --app=celery_config worker -l INFO
elif [[ "${1}" == "beat" ]]; then
  celery --app=celery_config beat
elif [[ "${1}" == "flower" ]]; then
  celery --app=celery_config:celery_app flower
 fi