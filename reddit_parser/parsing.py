import datetime
import os
import re
import requests
from sqlalchemy import and_, insert, select, update
from sqlalchemy.ext.asyncio import AsyncSession
import asyncpraw
import yt_dlp
from models import *
from s3_connection import s3
from config import BUCKET
from shared.loggin_setup import LoggerSetup

logger_setup = LoggerSetup('parsingReddit', __name__)
LOGGER = logger_setup.logger

client_id = "HKbsN1XhsLXtpOw7JHdWmg"
client_secret = "hsdaeUWYlSMcveVwDHckZ7-y7_uwVQ"
user_agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 YaBrowser/23.7.1.1140 Yowser/2.5 Safari/537.36'

class RedditParser:
    def __init__(self, session, 
                 client_id=client_id,
                 client_secret=client_secret,
                 user_agent=user_agent,
                 s3=s3) -> None:

        self.session: AsyncSession = session
        self.client_id = client_id
        self.client_secret = client_secret
        self.user_agent = user_agent
        self.s3 = s3
    
    async def clear_text(self, text):
        text = re.sub(r"[^0-9a-zA-Z]+", ' ', text)
        LOGGER.info(f'Clean_text:{text}')
        return text
    
    async def reddit_api(self):
        reddit = asyncpraw.Reddit(
            client_id=self.client_id,
            client_secret=self.client_secret,
            user_agent=self.user_agent,
        )
        return reddit
    
    async def list_reddits(self, reddit):
        stmt = select(Subreddits.title)
        res = await self.session.execute(stmt)
        res = res.fetchall()
        counter_all_data = 0 
        added_data = 0
        try:
            for i in res:
                async for subreddit in reddit.subreddits.search(i, limit=None):
                    counter_all, added_data_counter = await self.search_subreddit(i, subreddit)
                    counter_all_data += counter_all
                    added_data += added_data_counter
        except Exception as e:
            LOGGER.error(e)
        return counter_all_data, added_data
    
    async def search_subreddit(self, tittle, subreddit):
        counter_all = 0
        added_data_counter = 0
        try:
            async for submission in subreddit.search(tittle,
                                                    sort="relevance",
                                                    time_filter='day'
                                                    ):
                try:
                    published = datetime.fromtimestamp(int(submission.created_utc))

                    # LOGGER.warning(f'Новость\nНазвание запроса: {tittle}\nСообщение:{submission.title}\nВремя:{published}\nСсылка: {submission.url}')

                    if submission.media is not None:
                        LOGGER.info(f'submission.media: {submission.media}')
                        if "reddit_video" in submission.media:
                            video_url = submission.media['reddit_video']['fallback_url']
                            LOGGER.info(f'video_url: {video_url}')
                        else:
                            video_url = None
                    else:
                        video_url = None
                    
                    counter_all += 1
                    
                    formatted_published = published.strftime("%Y-%m-%d")
                    cleaned_text = await self.clear_text(submission.title)
                    
                    stmt = (
                        select(Posts)
                        .where(
                            and_(
                                Posts.title == cleaned_text,
                                Posts.published == published
                            )
                        )
                    )
                    post_in_db = await self.session.execute(stmt)
                    post_in_db = post_in_db.fetchone()
                    LOGGER.info(f'post_in_db: {post_in_db}')
                    
                    if post_in_db is None:                       
                        if not submission.is_self and submission.url.lower().endswith(('.jpg', '.jpeg', '.png', '.gif')):
                            file_name = f'{formatted_published}-{cleaned_text[:15]}.jpg'
                            response = requests.get(submission.url)
                            with open(file_name, 'wb') as f:
                                f.write(response.content)
                            
                            self.s3.upload_file(file_name, BUCKET, file_name)
                            os.remove(file_name)
                            
                            dict_info = {
                                "title": cleaned_text,
                                "published": published,
                                "info_vvst": {
                                    'Танки': 0,
                                    'БМП': 0,
                                    'Грузовики': 0
                                },
                                "path_image": file_name
                            }
                            
                            add_post = insert(Posts).values(
                                source=1,
                                title=dict_info['title'],
                                info_vvst=dict_info['info_vvst'],
                                published=dict_info['published'],
                                path_video=dict_info['path_image'],
                            )
                            LOGGER.warning(f'Новость\nДобавлена\nНазвание запроса: {tittle}\nСообщение:{submission.title}\nВремя:{published}\nСсылка: {submission.url}')

                        elif video_url is not None:
                            file_name = f'{formatted_published}-{cleaned_text[:15]}.mp4'
                            ydl_opts = {
                                'outtmpl': file_name
                            }
                            
                            with yt_dlp.YoutubeDL(ydl_opts) as ydl:
                                ydl.download([video_url])
                            
                            self.s3.upload_file(file_name, BUCKET, file_name)
                            os.remove(file_name)
                            
                            dict_info = {
                                "title": cleaned_text,
                                "published": published,
                                "info_vvst": {
                                    'Танки': 0,
                                    'БМП': 0,
                                    'Грузовики': 0
                                },
                                "path_video": file_name
                            }
                            
                            add_post = insert(Posts).values(
                                source=1,  # id  -- Reddit
                                title=dict_info['title'],
                                info_vvst=dict_info['info_vvst'],
                                published=dict_info['published'],
                                path_video=dict_info['path_video'],
                            )
                            LOGGER.warning(f'Новость\nДобавлена\nНазвание запроса: {tittle}\nСообщение:{submission.title}\nВремя:{published}\nСсылка: {submission.url}')

                        else:
                            continue
                        
                        await self.session.execute(add_post)
                        await self.session.commit()
                        added_data_counter += 1
                    else:
                        LOGGER.info(f'post in db: {cleaned_text}')
                except Exception as e:
                    LOGGER.error(e, exc_info=True)
            
        except Exception as e:
            LOGGER.error(e, exc_info=True)
        
        return counter_all, added_data_counter
    
    async def main(self):
        try:
            reddit = await self.reddit_api()
            os.makedirs('reddit_video', exist_ok=True)
            os.makedirs('reddit_image', exist_ok=True)
            counter_all_data, added_data = await self.list_reddits(reddit)
            LOGGER.warning(f'В результате парсинга реддита было найдено: {counter_all_data} новостей\nИз них: {added_data} добавлено')
        except Exception as e:
            LOGGER.error(e)