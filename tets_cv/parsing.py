import datetime
import os
import re
from sqlalchemy import and_, insert, select, update
from sqlalchemy.ext.asyncio import AsyncSession
import asyncpraw
import youtube_dl
from models import *
from s3_connection import s3
from config import BUCKET

client_id="HKbsN1XhsLXtpOw7JHdWmg"
client_secret="hsdaeUWYlSMcveVwDHckZ7-y7_uwVQ"
user_agent='Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 YaBrowser/23.7.1.1140 Yowser/2.5 Safari/537.36'



class Reddit_parser:
    def __init__(self, session, 
                 client_id=client_id,
                 client_secret=client_secret,
                 user_agent=user_agent,
                 s3 = s3) -> None:

        self.session: AsyncSession = session
        self.client_id = client_id
        self.client_secret = client_secret
        self.user_agent = user_agent
        self.s3 = s3
    
    async def clear_text(self, text):
        text = re.sub(r"[^0-9a-zA-Z]+", ' ', text)
        return text
    
    async def reddit_api(self):
        reddit = asyncpraw.Reddit(
            client_id=self.client_id,
            client_secret=self.client_secret,
            user_agent=self.user_agent,
    )
        return reddit
    
    async def list_reddits(self, reddit):
        stmt = select(Subreddits.title)
        res = await self.session.execute(stmt)
        res = res.fetchall()
        for i in res:
            print(i)
            await self.search_subreddit(reddit, i[0])
    

    async def search_subreddit(self,reddit, query):
        subreddits = reddit.subreddits.search(query)
        async for subreddit in subreddits:
            try:
                print(subreddit)
                async for submission in subreddit.search(query,
                                                        sort="relevance",
                                                        # time_filter ='month',
                                                        ):
                    try:
                        media_type = submission.media
                        if media_type != None:
                            video_url = submission.media['reddit_video']['fallback_url']
                            published = datetime.fromtimestamp(int(submission.created_utc))
                            print(video_url)

                            formatted_published = published.strftime("%Y-%m-%d")
                            cleaned_text = await self.clear_text(submission.title)
                            stmt = (
                                 select(Posts)
                                 .where(
                                    and_(
                                        Posts.title == cleaned_text,
                                        and_(Posts.published == published)
                                        )
                                ))
                            post_in_db = await self.session.execute(stmt)
                            await self.session.commit()
                            post_in_db = post_in_db.fetchone()
                            if post_in_db==None:
                                file_name = f'reddit_video/{formatted_published}-{cleaned_text[:15]}.mp4'
                                ydl_opts = {
                                            'outtmpl': file_name
                                            }
                                
                                with youtube_dl.YoutubeDL(ydl_opts) as ydl:
                                    ydl.download([video_url])

                                self.s3.upload_file(file_name, BUCKET, file_name)
                                self.s3.close()
                                os.remove(file_name)
                                dict_info = {
                                                "title": cleaned_text,
                                                "published": published,
                                                "info_vvst": {
                                                    'Танки':0,
                                                    'БМП':0,
                                                    'Грузовики':0
                                                },
                                                "path_video": file_name
                                            }
                                add_post = insert(Posts).values(source=1,  # id  -- Reddit
                                                                title=dict_info['title'],
                                                                info_vvst = dict_info['info_vvst'],
                                                                published=dict_info['published'],
                                                                path_video=dict_info['path_video'],
                                                                )
                                await self.session.execute(add_post)
                                await self.session.commit()
                            else:
                                print(f'post_in_db: {post_in_db}')
                    except Exception as e:
                        print(e)
                        pass
            
            except Exception as e:
                print(e)
    
    async def main(self):
        reddit = await self.reddit_api()
        await self.list_reddits(reddit)
        


