import cv2
from ultralytics import YOLO
from roboflow import Roboflow
from shared.settings import BUCKET
from sqlalchemy import and_, insert, select, update
from shared.models import *
from shared.postgre import session
from s3_connection import s3
from shared.loggin_setup import LoggerSetup

logger_setup = LoggerSetup('test_cv', __name__)
LOGGER = logger_setup.logger

LOGGER.info(f'BUCKET: {BUCKET}')

class CV_reddit:
    def __init__(self) -> None:
        self.roboflow = self.init_robflow() 
        self.train_predict = self.init_YOLO()  
        self.session = session


    def init_robflow(self):
        rf = Roboflow(api_key="f6DQitMGIm56iOEFJXcT")
        project = rf.workspace("rzd-na5fd").project("bvb-t60nb")
        model_mil = project.version(4).model
        return model_mil

    def init_YOLO(self):
        model_train = YOLO('yolov8n.pt')
        return model_train


    def search_frame(self):
        stmt = select(Posts)
        stmt = stmt.where(Posts.processed == False)
        res = self.session.execute(stmt)
        res = res.scalars().all()

        for i in res:
            try:
                get_object_response = s3.get_object(Bucket=BUCKET, Key=i.path_video)
                name_file = i.path_video.split('/')[-1] 
                with open(name_file, 'wb') as file:
                            file.write(get_object_response['Body'].read())
                LOGGER.info(f'Start processed Cv : name_file: {name_file}, id_pos: {i.id_post}')
                train, military =  self.cv(name_file)
                upd_post = update(Posts)
                upd_post = upd_post.where(Posts.id_post == i.id_post)
                upd_post = upd_post.values(
                    is_tank = military,
                    is_train = train,
                    processed = True,
                )
                self.session.execute(upd_post)
                self.session.commit()
                LOGGER.info(f'{i.id_post}: upd_post, is_tank : {military} is_train : {train} ')

            except Exception as e:
                LOGGER.error(f'Exception: {e}', exc_info=True)


    def cv(self, path_file:str):
        if path_file.split('.')[-1] !='mp4':
            cap = cv2.VideoCapture(path_file)
            success, frame = cap.read()
            result_train = self.train_predict.predict(frame) 
            for r in result_train:
                res = r.keypoints
                if res:
                     train = True
                else:
                     train = False
                      
            result_mil = self.roboflow.predict(frame, confidence=15, overlap=30)
            if result_mil.json()['predictions']:
                military = True
            else:
                military = False
            return train, military
        
        else:
            freg_frame = 300
            train = False
            military = False
            cap = cv2.VideoCapture(path_file)
            for count in range(freg_frame):
                LOGGER.info(f'count: {count}')
                success, frame = cap.read()
                result_train = self.train_predict.predict(frame)
                for r in result_train:
                    if r.boxes:
                        for box in r.boxes:
                            class_id = int(box.cls[0])
                            class_name = r.names[class_id]
                            if class_name == 'train':
                                train = True
                            else:
                                train = False

                result_mil = self.roboflow.predict(frame, confidence=15, overlap=30)
                
                if result_mil.json()['predictions']:
                    military = True
                else:
                    military = False

                if train == True and military == True:
                    return train, military
                
            return train, military
        

if __name__ == "__main__":
    import time
    cv_reddit = CV_reddit()
    while True:
        cv_reddit.search_frame()
        time.sleep(60)
